import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, logIn } from "./actions";

const Box = () => {
  const counter = useSelector(state => state.counter);
  const isLogged = useSelector(state => state.isLogged);
  const dispatch = useDispatch();

  return (
    <div>
      <h2>Box</h2>
      <p>Counter : {counter}</p>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      <button onClick={() => dispatch(logIn())}>Log In</button>
      {isLogged ? <p>Valuable information I shouldn't see</p> : ""}
    </div>
  );
};

export default Box;
