import React from "react";
import Box from "./Box";

function App() {
  return (
    <div className="App">
      <h1>Hello</h1>
      <Box />
    </div>
  );
}

export default App;
